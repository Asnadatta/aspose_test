class Tenant < ActiveRecord::Base
	validates :name, :subdomain, presence: true
	has_many :posts, dependent: :destroy
end
