class Post < ActiveRecord::Base
  belongs_to :tenant
  validates :tenant, :name, presence: true
end
