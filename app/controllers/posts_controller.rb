class PostsController < ApplicationController
	before_filter :set_tenant
  before_filter :set_post, only:[:show]

	def index
		@posts = @tenant.posts
	end

	private
		def set_post
			@post = Post.find_by(tenant_id: @tenant.id, id: params[:id])
			redirect_to posts_path, alert: "Post could not be found" if @post.blank?
		end
end
