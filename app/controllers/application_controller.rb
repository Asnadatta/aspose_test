class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  def set_tenant
    @tenant = Tenant.find_by_subdomain(request.subdomain)
    redirect_to root_url(:subdomain => nil) if @tenant.blank?
  end
end
