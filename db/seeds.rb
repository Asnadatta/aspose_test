# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
AdminUser.create(email: "sabir@aspose.com", password: '12345678')
t1 = Tenant.create(subdomain: "aspose", name: "Aspose")
t2 = Tenant.create(subdomain: "gitlab", name: "Gitlab")
for i in 0..10 do
	t1.posts.create(name: Faker::Commerce.department, description: Faker::Lorem.paragraphs.join(". "))
	t2.posts.create(name: Faker::Commerce.department, description: Faker::Lorem.paragraphs.join(". "))
end